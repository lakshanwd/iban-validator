# International Bank Account Number (IBAN) Validator

### Description
This is a simple application that validates International Bank Account Numbers (IBANs).

### Prerequisites
* golang 1.18
* docker
* make

### Usage
* Use can start the application with the following command:
```bash
make setup
make run
```
visit http://localhost:8000/ to see the swagger documentation.