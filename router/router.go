package router

import (
	"iban-validator/controller"

	_ "iban-validator/docs"

	"github.com/go-chi/chi/v5"
	cm "github.com/go-chi/chi/v5/middleware"
	httpSwagger "github.com/swaggo/http-swagger"
)

// NewRouter creates a new router
func NewRouter(iban controller.IBANController) *chi.Mux {
	router := chi.NewRouter()
	router.Use(cm.RequestID, cm.Logger, cm.Recoverer, cm.Heartbeat("/ping"))
	router.Post("/check", iban.IsValidIBAN)
	router.Mount("/", httpSwagger.WrapHandler)
	return router
}
