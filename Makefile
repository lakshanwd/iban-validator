setup:
	go install github.com/swaggo/swag/cmd/swag@latest
	go mod download

run: swagger
	go run ./main.go

build: swagger
	go mod tidy
	go build -o main

test:
	go test ./...

swagger:
	swag init