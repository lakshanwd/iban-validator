package main

import (
	"flag"
	"iban-validator/application"
	"log"
)

// @title International Bank Account Number (IBAN) Validator
// @version 1.0
// @description validates International Bank Account Number (IBAN)
// @BasePath /
func main() {
	var port int
	flag.IntVar(&port, "port", 8000, "Port to listen on")
	flag.Parse()
	app := application.NewApp(port)
	log.Println("Starting server on port", port)
	err := app.Run()
	if err != nil {
		log.Fatal(err, "unable to start")
	}
}
