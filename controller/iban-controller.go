package controller

import (
	"iban-validator/dto/request"
	"iban-validator/dto/response"
	"iban-validator/service"
	"net/http"

	"github.com/go-chi/render"
)

// IBANController definition of the IBANController interface
type IBANController interface {
	IsValidIBAN(w http.ResponseWriter, r *http.Request)
}

// iban_controller_impl implements IBANController interface
type iban_controller_impl struct {
	ibanSvc service.IBANService
}

// NewIBANController creates a new instance of the IBANController interface
func NewIBANController(ibavSvc service.IBANService) IBANController {
	return &iban_controller_impl{ibanSvc: ibavSvc}
}

// IsValidIBAN godoc
// @Summary Checks if the given IBAN is valid or not
// @Description Checks if the given IBAN is valid or not
// @Tags IBAN
// @Accept json
// @Produce json
// @Param iban body request.IBANIsValidRequest true "IBAN to validate"
// @Success 200 {object} response.IBANIsValidResponse
// @Failure 400 {object} response.GeneralErrorResponse
// @Router /check [post]
func (ctrl *iban_controller_impl) IsValidIBAN(w http.ResponseWriter, r *http.Request) {
	var req request.IBANIsValidRequest
	err := render.DefaultDecoder(r, &req)
	if err != nil {
		render.Status(r, http.StatusBadRequest)
		render.DefaultResponder(w, r, response.GeneralErrorResponse{Error: err.Error()})
		return
	}
	err = req.Validate()
	if err != nil {
		render.Status(r, http.StatusBadRequest)
		render.DefaultResponder(w, r, response.GeneralErrorResponse{Error: err.Error()})
		return
	}

	// validate the IBAN
	isValid := ctrl.ibanSvc.IsValidIBAN(req.IBAN)

	// return the result
	render.DefaultResponder(w, r, response.IBANIsValidResponse{IsValid: isValid, Success: true})
}
