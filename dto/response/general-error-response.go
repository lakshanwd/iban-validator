package response

// GeneralErrorResponse is the response for general errors
type GeneralErrorResponse struct {
	Error   string `json:"error"`
	Success bool   `json:"success"`
}
