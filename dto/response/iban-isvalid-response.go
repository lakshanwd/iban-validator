package response

// IBANIsValidResponse is the response for the IBANIsValidRequest
type IBANIsValidResponse struct {
	IsValid bool `json:"isValid"`
	Success bool `json:"success"`
}
