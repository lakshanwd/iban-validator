package request

import "errors"

// IBANIsValidRequest godoc
// @Summary Checks if the given IBAN is valid or not
type IBANIsValidRequest struct {
	IBAN string `json:"iban"`
}

// Validate validates the request
func (req *IBANIsValidRequest) Validate() error {
	if req.IBAN == "" {
		return errors.New("IBAN is required")
	}
	return nil
}
