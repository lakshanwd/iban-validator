package request

// Validator is the interface for validating requests
type Validator interface {

	// Validate validates the request
	Validate() error
}
