package service

import (
	"strconv"
	"strings"
)

//go:generate mockery --name=IBANService
type IBANService interface {
	// IsValidIBAN checks if the given IBAN is valid or not
	IsValidIBAN(iban string) bool
}

type iban_service_impl struct{}

// NewIBANService returns a new IBANService
func NewIBANService() IBANService {
	return &iban_service_impl{}
}

// IsValidIBAN implements the IBANService interface
func (svc *iban_service_impl) IsValidIBAN(iban string) bool {
	// remove spaces
	iban = strings.Replace(iban, " ", "", -1)

	//make upper case
	iban = strings.ToUpper(iban)

	//re-arrange the IBAN
	iban = iban[4:] + iban[0:4]
	var nums []string
	for _, c := range iban {
		switch {
		case 'A' <= c && c <= 'Z':
			nums = append(nums, strconv.Itoa(int(c-'A'+10)))
		default:
			i, err := strconv.Atoi(string(c))
			if err != nil {
				return false
			}
			nums = append(nums, strconv.Itoa(i))
		}
	}

	iban = strings.Join(nums, "")
	res, err := calculateMod97(iban)
	if err != nil {
		return false
	}
	return res == 1
}

// calculateMod97 calculates the mod97 of the given IBAN
func calculateMod97(iban string) (int, error) {
	prefix, err := strconv.Atoi(iban[:2])
	if err != nil {
		return 0, err
	}
	return mod97(prefix, iban[2:])
}

func mod97(prefix int, rest string) (int, error) {
	if len(rest) <= 7 {
		num, err := strconv.Atoi(strconv.Itoa(prefix) + rest)
		if err != nil {
			return 0, err
		}
		return num % 97, nil
	}
	num, err := strconv.Atoi(strconv.Itoa(prefix) + rest[:7])
	if err != nil {
		return 0, err
	}
	return mod97(num%97, rest[7:])
}
