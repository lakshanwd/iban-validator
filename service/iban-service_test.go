package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsValidIBAN(t *testing.T) {

	t.Run("valid IBAN", func(t *testing.T) {
		svc := NewIBANService()
		result := svc.IsValidIBAN("DE89370400440532013000")
		assert.True(t, result)
	})

	t.Run("invalid IBAN", func(t *testing.T) {
		svc := NewIBANService()
		result := svc.IsValidIBAN("DE8937040044053201300")
		assert.False(t, result)
	})

}
