package application

import (
	"fmt"
	"iban-validator/controller"
	"iban-validator/router"
	"iban-validator/service"
	"net/http"
)

// app is the application struct
type app struct {
	port            int
	iban_controller controller.IBANController
}

// NewApp creates a new instance of the App
func NewApp(port int) *app {
	// init dependancies
	ibanSvc := service.NewIBANService()

	// init controllers
	ibanCtrl := controller.NewIBANController(ibanSvc)

	// init app
	return &app{port: port, iban_controller: ibanCtrl}
}

// Run runs the application
func (app *app) Run() error {
	r := router.NewRouter(app.iban_controller)
	return http.ListenAndServe(fmt.Sprintf(":%d", app.port), r)
}
